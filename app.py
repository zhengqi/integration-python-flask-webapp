from datetime import datetime
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
import numpy as np
import os
app = Flask(__name__)

def integration(lower_bound, upper_bound, N):
    x = np.linspace(lower_bound, upper_bound, N)
    y = abs(np.sin(x))
    dx = x[1] - x[0]
    return np.sum(y*dx)

def test_integration(lb, ub):
    result = []
    for n in [10, 100, 1000, 10000, 100000, 1000000, 10000000]:
        val = integration(lb, ub, n)
        result.append(str(val))
    return ' >> '.join(result)

@app.route('/')
def index():
   print('Request for index page received')
   return render_template('index.html')

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/hello', methods=['POST'])
def hello():
   ub = request.form.get('upper')
   lb = request.form.get('lower')

   if (ub and lb):
       value = test_integration(float(lb), float(ub))
       print('Request for hello page received with name=%s' % value)
       return render_template('hello.html', name = value)
   else:
       print('Request for hello page received with no name or blank name -- redirecting')
       return redirect(url_for('index'))


if __name__ == '__main__':
   app.run()