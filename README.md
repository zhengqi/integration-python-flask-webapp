# Description

This is the sample Flask application modified from the Azure Quickstart [Deploy a Python (Django or Flask) web app to Azure App Service](https://docs.microsoft.com/en-us/azure/app-service/quickstart-python).  For instructions on how to create the Azure resources and deploy the application to Azure, refer to the Quickstart article.

Given an interval lower and upper, the programme breaks up the interval into N subintervals(N = 10, 100, 100, 1000, 10k, 100k, 1M), compute the area of the rectangle at each subinterval and add them all up. For example, if you give as input 0 and 3.14159 (which is approximately π), you should get 1.99… which is close to 2.